#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=$?
source /etc/profile.d/modules.sh

module load mpi/openmpi-x86_64

ompi_info
let ret=$ret+$?

mpirun --version
let ret=$ret+$?

mpirun --allow-run-as-root hostname
let ret=$ret+$?

mpirun --allow-run-as-root uptime
let ret=$ret+$?

exit $ret
