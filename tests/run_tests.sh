#!/bin/bash

# when running this in 1minutetip the PATH must be specified to execute
# in the local directory.
echo "Setting path to local directory"
PATH=$PATH:$(pwd)

# simple sanity test
sanity.sh
exit $?
